import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';

const electron = window.require('electron');

const App = () => {
  const [message] = useState("React Hooks - Electron - Es Lint - Boilerplate - 2018");

  console.log(electron);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {message}
      </header>
    </div>
  );
}

export default App;
